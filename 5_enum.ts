enum Modifiers {
  Private,
  Protected,
  Public
}

const private = Modifiers.Private
const privateReverse = Modifiers[0]

console.log(private)
console.log(privateReverse)

enum ModifiersWithValues {
  Private = 'private',
  Protected = 'protected',
  Public = 'public'
}

const public = ModifiersWithValues.Public
console.log(public)
